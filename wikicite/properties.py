import argparse
import pathlib as Path
import time
from datetime import date

import pandas as pd
import pywikibot
import requests
from requests.exceptions import HTTPError

HEADERS = {"User-Agent": "Add-Letterboxd-ID"}
CUR_DIR = Path(__file__).parent.absolute()


def properties():
    def getTotal(p31):
        with (CUR_DIR / "total.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)
        return getData(query, p31=p31)

    def getCounts(input, queryFile="property.sparql"):
        counts = {}
        with (CUR_DIR / queryFile).open() as r:
            query = r.readlines()
            query = "".join(query)
        # counts["P31"] = getTotal(input["P31"])
        props = ["P31"] + input["props"]
        for prop in props:
            counts[prop] = getData(query, prop, input["P31"])

        return counts

    def makeDataframe(counts):
        df = pd.DataFrame.from_dict(data=counts, orient="index", columns=["count"])

        df["percentage"] = df["count"] / df["count"]["P31"]
        # df["top items to edit"]
        return df

    def makeOutput(data, df, articles=False):
        output = "\n\n== {{Q'''|" + data["P31"] + "}} ==\n"
        if articles:
            output += "\nSampled 400k due to query timeout\n"
        output += """\n{| class="wikitable sortable"
! Property !! Count !! Percentage\n"""

        for p, cols in df.iterrows():
            output += (
                "|-\n| {{P'|"
                + p
                + "}} || "
                + "{:,.0f}".format(cols["count"])
                + " || "
                + "{:.2f}%".format(cols["percentage"] * 100)
                + "\n"
            )

        output += "|}"

        return output

    articles = {
        "P31": "Q13442814",
        "props": [
            "P50",
            "P2860",
            "P2093",
            "P921",
            "P98",
            "P136",
            "P577",
            "P1476",
            "P1680",
            "P1433",
            "P478",
            "P433",
            "P304",
            "P1104",
            "P275",
            "P3931",
            "P953",
            "P854",
            "P407",
            "P373",
            "P859",
            "P1325",
            "P4032",
            "P2507",
            "P5824",
            "P4510",
        ],
    }
    journals = {
        "P31": "Q5633421",
        "props": [
            "P1476",
            "P123",
            "P291",
            "P98",
            "P275",
            "P407",
            "P361",
            "P437",
            "P571",
            "P576",
            "P872",
            "P921",
            "P373",
            "P1160",
            "P155",
            "P1366",
            "P1019",
            "P127",
            "P9767",
            "P9901",
        ],
    }
    proceedings = {
        "P31": "Q1143604",
        "props": [
            "P1476",
            "P407",
            "P98",
            "P478",
            "P179",
            "P577",
            "P921",
            "P123",
            "P953",
        ],
    }
    proceedingsSeries = {
        "P31": "Q27785883",
        "props": ["P1476", "P407", "P921", "P123", "P953", "P9901"],
    }
    supplements = {"wdt": "P9234", "props": ["P98", "P859", "P1433", "P9234", "P9767"]}
    theses = {
        "P31": "Q1266946",
        "props": ["P50", "P1476", "P407", "P921", "P953", "P4101", "P9161"],
    }
    books = {
        "P31": "Q571",
        "props": [
            "P50",
            "P655",
            "P98",
            "P110",
            "P123",
            "P1476",
            "P1680",
            "P275",
            "P953",
            "P407",
            "P921",
            "P373",
            "P3931",
            "P6338",
            "P9191",
            "P9767",
            "P9969",
            "P10836",
            "P10837",
        ],
    }
    authors = {
        "wdt": "P50",
        "props": ["P1559", "P1416", "P968", "P101", "P800", "P373", "P835"],
    }
    publishers = {"P31": "Q2085381", "props": ["P159", "P101", "P373", "P4016"]}

    today = date.today().strftime("%d %B %Y")
    finalOutput = (
        "= Summary =\n\n{{/Summary}}\n\n= Properties =\n\nUpdated: " + today + "\n"
    )

    # counts = getCounts(articles, "articles.sparql")
    # df = makeDataframe(counts)
    # finalOutput += makeOutput(data, df, articles=True)

    for data in [journals, proceedings, proceedingsSeries, theses, books, publishers]:
        counts = getCounts(data)
        df = makeDataframe(counts)
        finalOutput += makeOutput(data, df)

    return finalOutput


def externalIdentifiers():
    def getExternalIdentifiers():
        output = {}
        with (CUR_DIR / "external-id.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)

        for o in data["results"]["bindings"]:
            output[o["item"]["value"][31:]] = {"total": int(o["numrecords"]["value"])}

        return output

    def getCoverage(ids):
        with (CUR_DIR / "coverage.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        for prop in ids.keys():
            ids[prop]["coverage"] = getData(query, prop)

        df = pd.DataFrame.from_dict(
            data=ids, orient="index", columns=["coverage", "total"]
        )

        df["percentage"] = df["coverage"] / df["total"]
        return df

    def makeOutput(df):
        today = date.today().strftime("%d %B %Y")
        output = (
            """\n\n= External Identifiers =\n\nNumber of items with a given external identifier. Updated: """
            + today
            + """\n\n{| class="wikitable sortable"
! Property !! Items on Wikidata !! [[Property:P4876|Total in source]] !! Coverage\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "|-\n| {{P'|"
                + p
                + "}} || "
                + "{:,.0f}".format(cols["coverage"])
                + " || "
                + "{:,.0f}".format(cols["total"])
                + " || "
                + "{:.2f}%".format(cols["percentage"] * 100)
                + "\n"
            )

        output += "|}"

        return output

    ids = getExternalIdentifiers()
    coverage = getCoverage(ids)
    output = makeOutput(coverage)

    return output


def summary():
    def getCounts(props, special):
        with (CUR_DIR / "summary.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = {k: {} for k in props}

        for prop in data.keys():
            data[prop]["count"] = getDataFromAPI(f"haswbstatement:{prop}")

        for d in special:
            data[d["name"]] = {"count": getDataFromAPI(d["search"])}

        df = pd.DataFrame.from_dict(data=data, orient="index", columns=["count"])

        df["without"] = df["count"]["P31"] - df["count"]
        df["percentage"] = df["count"] / df["count"]["P31"]
        return df

    def makeOutput(props, special, df):
        today = date.today().strftime("%d %B %Y")
        output = f"Of the '''{df['count']['P31']:,}''' items which are {{{{P'|P31}}}} of '''{{{{Q'''|Q13442814}}}}''':\n\n"
        output += '{| class="wikitable sortable"\n'
        output += "!  !! With property !! Without property !! Coverage \n"
        for p in props[1:]:
            output += "|-\n! {{P'|" + p + "}}\n"
            output += "| {:,.0f}\n".format(df["count"][p])
            output += "| {:,.0f}\n".format(df["without"][p])
            output += "| {:.1f}%\n".format(df["percentage"][p] * 100)

        for s in special:
            output += f"|-\n! {s['name']}\n"
            output += "| {:,.0f}\n".format(df["count"][s["name"]])
            output += "| {:,.0f}\n".format(df["without"][s["name"]])
            output += "| {:.1f}%\n".format(df["percentage"][s["name"]] * 100)

        output += "|}\n"
        output += f"Updated: {today}."

        return output

    props = ["P31", "P356", "P698", "P921", "P407", "P50", "P2093"]  # , "P1476"
    special = [
        {
            "name": "Only {{P'|P50}}, no {{P'|P2093}}",
            "search": "haswbstatement:P50 -haswbstatement:P2093",
        },
        {
            "name": "Only {{P'|P2093}}, no {{P'|P50}}",
            "search": "-haswbstatement:P50 haswbstatement:P2093",
        },
    ]
    counts = getCounts(props, special)
    output = makeOutput(props, special, counts)

    return output


def getData(query, prop="", p31=""):
    if p31 and prop:
        data = runQuery(query.format(prop=prop, p31=p31))
    elif p31:
        data = runQuery(query.format(p31=p31))
    else:
        data = runQuery(query.format(prop=prop))

    return int(data["results"]["bindings"][0]["count"]["value"])


def runQuery(query):
    url = "https://query.wikidata.org/sparql"
    params = {"query": query, "format": "json"}
    try:
        response = requests.get(url, params=params, headers=HEADERS)
        return response.json()
    except HTTPError as e:
        print(response.text)
        print(e.response.text)
        print(query)
        return {"results": {"bindings": []}}
    except BaseException as err:
        print(query)
        print(f"Unexpected {err}, {type(err)}")
        raise


def getDataFromAPI(search):
    url = "https://www.wikidata.org/w/api.php?action=query&list=search&srsearch=haswbstatement:P31=Q13442814%20{search}&srlimit=1&format=json"

    response = requests.get(url.format(search=search), headers=HEADERS)
    return response.json()["query"]["searchinfo"]["totalhits"]


def makeEdit(page, string):
    site = pywikibot.Site("wikidata", "wikidata")
    site.login()
    page = pywikibot.Page(site, page)

    page.text = string
    page.save("Update statistics")


def timer(tick, msg=""):
    print("--- %s %.3f seconds ---" % (msg, time.time() - tick))
    return time.time()


def defineArgParser():
    """Creates parser for command line arguments"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument("-t", "--test", action="store_true")

    return parser


if __name__ == "__main__":
    argParser = defineArgParser()
    clArgs = argParser.parse_args()

    tick = time.time()
    output = ""
    output += properties()
    output += externalIdentifiers()

    summarystr = ""
    summarystr += summary()

    if not clArgs.test:
        makeEdit("Wikidata:WikiCite/Statistics", output)
        makeEdit("Wikidata:WikiCite/Statistics/Summary", summarystr)
        pass
    else:
        with (CUR_DIR / "output.txt").open("w") as w:
            w.write(output)
        with (CUR_DIR / "output-summary.txt").open("w") as w:
            w.write(summarystr)
    timer(tick)
