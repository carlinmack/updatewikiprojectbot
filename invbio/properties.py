import argparse
import pathlib as Path
import time
from datetime import date

import pandas as pd
import pywikibot
import requests
from requests.exceptions import HTTPError

HEADERS = {"User-Agent": "Add-Letterboxd-ID"}
CUR_DIR = Path(__file__).parent.absolute()


def types():
    def getTypes():
        output = {}
        with (CUR_DIR / "types.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)

        for o in data["results"]["bindings"]:
            output[o["type"]["value"][31:]] = int(o["count"]["value"])

        return output

    def makeOutput(data):
        today = date.today().strftime("%d %B %Y")
        output = (
            """== Types ==\n\nTop ten {{P|31}} maintained by WikiProject Invasion Biology. Updated: """
            + today
            + """\n\n{| class="wikitable sortable"
! Type !! Count maintained by WikiProject Invasion Biology\n"""
        )
        for k, v in data.items():
            output += "|-\n| {{Q'''|" + k + "}} || " + "{:,.0f}".format(v) + "\n"

        output += "|}"

        return output

    d = getTypes()
    output = makeOutput(d)

    return output


def hypotheses():
    def getHypotheses():
        output = {}
        with (CUR_DIR / "hypotheses.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)

        for o in data["results"]["bindings"]:
            output[o["hypothesis"]["value"][31:]] = int(o["count"]["value"])

        return output

    def makeOutput(data):
        today = date.today().strftime("%d %B %Y")
        output = (
            """\n\n== Hypotheses ==\n\nHypotheses related to invasion biology and their usage. Updated: """
            + today
            + """\n\n{| class="wikitable sortable"
|+ Hypotheses by number of papers
|-
! Hypothesis !! Count\n"""
        )
        for k, v in data.items():
            output += "|-\n| {{Q'''|" + k + "}} || " + "{:,.0f}".format(v) + "\n"

        output += "|}"

        return output

    d = getHypotheses()
    output = makeOutput(d)

    return output


def properties():
    def getTotal(p31):
        with (CUR_DIR / "total.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)
        return getData(query, p31=p31)

    def getCounts(input):
        counts = {}
        with (CUR_DIR / "property.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)
        # counts["P31"] = getTotal(input["P31"])
        props = ["P31"] + input["props"]
        for prop in props:
            counts[prop] = getData(query, prop, input["P31"])

        return counts

    def makeDataframe(counts):
        df = pd.DataFrame.from_dict(data=counts, orient="index", columns=["count"])

        df["percentage"] = df["count"] / df["count"]["P31"]
        # df["top items to edit"]
        return df

    def makeOutput(data, df):
        output = (
            "\n\n=== {{Q'''|"
            + data["P31"]
            + "}} ===\n"
            + """\n{| class="wikitable sortable"
! Property !! Count !! Percentage\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "|-\n| {{P'|"
                + p
                + "}} || "
                + "{:,.0f}".format(cols["count"])
                + " || "
                + "{:.2f}%".format(cols["percentage"] * 100)
                + "\n"
            )

        output += "|}"

        return output

    articles = {
        "P31": "Q13442814",
        "props": [
            "P50",
            "P2860",
            "P2093",
            "P921",
            "P98",
            "P136",
            "P577",
            "P1476",
            "P1680",
            "P1433",
            "P478",
            "P433",
            "P304",
            "P1104",
            "P275",
            "P3931",
            "P953",
            "P854",
            "P407",
            "P373",
            "P859",
            "P1325",
            "P4032",
            "P2507",
            "P5824",
            "P4510",
        ],
    }
    journals = {
        "P31": "Q5633421",
        "props": [
            "P1476",
            "P123",
            "P291",
            "P98",
            "P275",
            "P407",
            "P361",
            "P437",
            "P571",
            "P576",
            "P872",
            "P921",
            "P373",
            "P1160",
            "P155",
            "P1366",
            "P1019",
            "P127",
            "P9767",
            "P9901",
        ],
    }
    proceedings = {
        "P31": "Q1143604",
        "props": [
            "P1476",
            "P407",
            "P98",
            "P478",
            "P179",
            "P577",
            "P921",
            "P123",
            "P953",
        ],
    }
    proceedingsSeries = {
        "P31": "Q27785883",
        "props": ["P1476", "P407", "P921", "P123", "P953", "P9901"],
    }
    supplements = {"wdt": "P9234", "props": ["P98", "P859", "P1433", "P9234", "P9767"]}
    theses = {
        "P31": "Q1266946",
        "props": ["P50", "P1476", "P407", "P921", "P953", "P4101", "P9161"],
    }
    books = {
        "P31": "Q571",
        "props": [
            "P50",
            "P655",
            "P98",
            "P110",
            "P123",
            "P1476",
            "P1680",
            "P275",
            "P953",
            "P407",
            "P921",
            "P373",
            "P3931",
            "P6338",
            "P9191",
            "P9767",
            "P9969",
            "P10836",
            "P10837",
        ],
    }
    authors = {
        "wdt": "P50",
        "props": ["P1559", "P1416", "P968", "P101", "P800", "P373", "P835"],
    }
    publishers = {"P31": "Q2085381", "props": ["P159", "P101", "P373", "P4016"]}

    today = date.today().strftime("%d %B %Y")
    finalOutput = "\n\n== Properties ==\n\nUpdated: " + today + "\n"

    for data in [
        # articles,
        journals,
        proceedings,
        proceedingsSeries,
        theses,
        books,
        publishers,
    ]:
        counts = getCounts(data)
        df = makeDataframe(counts)
        if df["count"]["P31"] == 0:
            finalOutput += (
                "\n\nNo {{Q|"
                + data["P31"]
                + "}} maintained by Wikiproject:Invasion Biology\n"
            )
        else:
            finalOutput += makeOutput(data, df)

    return finalOutput


def externalIdentifiers():
    def getTotal(p31):
        with (CUR_DIR / "total.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)
        return getData(query, p31=p31)

    def getExternalIdentifiers():
        output = {}
        with (CUR_DIR / "external-id.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)
        total = getTotal("Q13442814")

        for o in data["results"]["bindings"]:
            output[o["item"]["value"][31:]] = {"total": total}

        return output

    def getCoverage(ids):
        with (CUR_DIR / "coverage.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        for prop in ids.keys():
            ids[prop]["coverage"] = getData(query, prop)

        df = pd.DataFrame.from_dict(
            data=ids, orient="index", columns=["coverage", "total"]
        )

        df["percentage"] = df["coverage"] / df["total"]
        return df

    def makeOutput(df):
        today = date.today().strftime("%d %B %Y")
        output = (
            """\n\n== External Identifiers ==\n\nNumber of items with a given external identifier. Updated: """
            + today
            + """\n\n{| class="wikitable sortable"
! Property !! Items maintained by WikiProject: Invasion Biology !! Percentage of papers maintained by WikiProject: Invasion Biology\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "|-\n| {{P'|"
                + p
                + "}} || "
                + "{:,.0f}".format(cols["coverage"])
                + " || "
                + "{:.2f}%".format(cols["percentage"] * 100)
                + "\n"
            )

        output += "|}"

        return output

    ids = getExternalIdentifiers()
    coverage = getCoverage(ids)
    output = makeOutput(coverage)

    return output


def propertyRelatedToInvasive():
    def get():
        output = {}
        with (CUR_DIR / "related-to-invasive-species.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        data = runQuery(query)

        for o in data["results"]["bindings"]:
            output[o["item"]["value"][31:]] = {"total": int(o["numrecords"]["value"])}

        return output

    def getCoverage(ids):
        with (CUR_DIR / "coverage.sparql").open() as r:
            query = r.readlines()
            query = "".join(query)

        for prop in ids.keys():
            ids[prop]["coverage"] = getData(query, prop)

        df = pd.DataFrame.from_dict(
            data=ids, orient="index", columns=["coverage", "total"]
        )

        df["percentage"] = df["coverage"] / df["total"]
        return df

    def makeOutput(df):
        today = date.today().strftime("%d %B %Y")
        output = (
            """\n\n= {{Q'''|Q58788038}} =\n\nNumber of items with a given external identifier. Updated: """
            + today
            + """\n\n{| class="wikitable sortable"
! Property !! Items on Wikidata !! [[Property:P4876|Total in source]] !! Coverage\n"""
        )
        for p, cols in df.iterrows():
            output += (
                "|-\n| {{P'|"
                + p
                + "}} || "
                + "{:,.0f}".format(cols["coverage"])
                + " || "
                + "{:,.0f}".format(cols["total"])
                + " || "
                + "{:.2f}%".format(cols["percentage"] * 100)
                + "\n"
            )

        output += "|}"

        return output

    ids = get()
    coverage = getCoverage(ids)
    output = makeOutput(coverage)

    return output


def getData(query, prop="", p31=""):
    if p31 and prop:
        data = runQuery(query.format(prop=prop, p31=p31))
    elif p31:
        data = runQuery(query.format(p31=p31))
    else:
        data = runQuery(query.format(prop=prop))

    return int(data["results"]["bindings"][0]["count"]["value"])


def runQuery(query):
    url = "https://query.wikidata.org/sparql"
    params = {"query": query, "format": "json"}
    try:
        response = requests.get(url, params=params, headers=HEADERS)
        return response.json()
    except HTTPError as e:
        print(response.text)
        print(e.response.text)
        print(query)
        return {"results": {"bindings": []}}
    except BaseException as err:
        print(query)
        print(f"Unexpected {err}\n")
        print(f"{type(err)}\n")
        raise


def makeEdit(page, string):
    site = pywikibot.Site("wikidata", "wikidata")
    site.login()
    page = pywikibot.Page(site, page)

    page.text = string
    page.save("Update statistics")


def timer(tick, msg=""):
    print("--- %s %.3f seconds ---" % (msg, time.time() - tick))
    return time.time()


def defineArgParser():
    """Creates parser for command line arguments"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument("-t", "--test", action="store_true")

    return parser


if __name__ == "__main__":
    argParser = defineArgParser()
    clArgs = argParser.parse_args()

    tick = time.time()
    output = ""
    output += types()
    output += hypotheses()
    output += properties()
    output += propertyRelatedToInvasive()
    output += externalIdentifiers()
    output += (
        "\n== More details ==\n\n* [[Wikidata:WikiProject Invasion biology/Statistics/Integraality]]"
        ""
    )

    if not clArgs.test:
        makeEdit("Wikidata:WikiProject_Invasion_biology/Statistics", output)
    else:
        with (CUR_DIR / "output.txt").open("w") as w:
            w.write(output)
    timer(tick)
