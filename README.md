## Update Wikiprojects on Wikidata

Usage: `python properties.py [-t]`


### To run on toolforge

Login as the tool account:
```
ssh myuser@login.toolforge.org
myuser@tools-sgebastion-10$ become updatewikiprojectbot
updatewikiprojectbot@tools-sgebastion-10$
```

Build the image:
```
updatewikiprojectmovies@tools-sgebastion-10$ toolforge build start https://gitlab.com/carlinmack/updatewikiprojectbot
... takes a bit, has to finish ok
```

Create the job if it does not exist (will pick the new image on the next run
if the job existst already):
```
updatewikiprojectbot@tools-sgebastion-10$ toolforge jobs run \
    --schedule "0 11 * * 1" \
    --image tool-updatewikiprojectbot/tool-updatewikiprojectbot:latest \
    --command "updatewikiprojectbot" \
    update-wikiprojectbot-job
```

Note that you you'll have to delete and re-create the job if you want to change
any parameters there (ex. image, command or schedule).
